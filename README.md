A configuration for [OpenBootstrap](https://gitlab.com/popularis-os/openbootstrap) that automates the building of a Linux system based on [Linux From Scratch](http://www.linuxfromscratch.org/index.html) version 9.1.

- This configuration uses the LZMA (xz) compression format wherever possible, falling back to GZip in cases where LZMA isn't available. Packages are downloaded directly from the download sites provided by the authors/maintainers rather than the LFS mirror wherever possible.
- Some packages have been updated from the versions stated in the LFS book because new, backwards-compatible minor versions of those packages have come out since the book was released. Also, the configuration has been adapted to not require root access. Otherwise this configuration tries to stay as close to stock LFS as possible.
- The default kernel version is Linux 5.4.49 LTS. It is generally safe to bump this to the latest stable kernel without making any further modifications.
